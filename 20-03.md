# 采购退货出库

---

## 新建采购退货出库单

采购退货需要和采购入库对应，在新建采购退货出库单的时候，首先需要选择对哪个采购入库单进行退货业务处理。
![](/assets/20-03.png)
选择好要处理的采购入库单后，就可以录入退货数据了。
![](/assets/20-03.1.png)
## 编辑采购退货出库单

编辑的时候，只能修改退货数据。

因为在新建采购退货出库单的时候已经选择了对应的采购入库单，所以在编辑的时间就不能再变更对应的采购入库单。

## 查看采购退货出库单

采购退货出库单被提交后，就不能再被编辑修改，但依旧可以查看信息。

## 删除采购退货出库单

采购退货出库单在没有提交之前可以从系统中删除。

## 提交采购退货出库单

采购退货出库单提交后，会实时修改库存账数据。

同时，对应的采购入库单的状态会被修改为“已退货”。

## 导出

采购退货出库单生成PDF文件，可以供打印使用。

## 帮助

查看采购退货库的操作方法以及操作流程

## 关闭

点击关闭，返回首页
